#!/usr/bin/python3.5

import sys
import pandas as pd
from itertools import groupby
from textblob import TextBlob

def Main():
    data = getdata()
    highrelevance(data['finding1'])
    inappropriate(data['finding2'])
    sentiment(data['finding3'])
    linking(data['finding4'])

def getdata():
    map_data = {'finding1':[], 'finding2':[], 'finding3':[], 'finding4':[]}
    for line in sys.stdin:
        finding, list = line.split("#",1)
        if finding in map_data:
            map_data[finding].append(list)
    return map_data

def highrelevance(list):
    video_id = []
    duration = []
    dummy = []
    for l in list:
        dummy.append(l.strip("\n"))
    for l in dummy:
        video, time = l.split("#",1)
        video_id.append(video)
        duration.append(time)
    for i in range(len(video_id)):
        print(video_id[i],duration[i])

def inappropriate(list1):
    total = 0
    my = []
    theme = []
    cctext = []
    appropriate = []
    inappropriate = []
    #print(list1)
    for line1 in list1:
        #print(line1)
        my.append(line1.strip("\n"))
        #print(my)
    for l in my:
        theme1, cctext1 = l.split("#", 1)
        #print(theme1)
        theme.append(theme1)
        cctext.append(cctext1)
    #print(theme)
    for i in theme:
        #print(i)
        j = cctext[total]
        #print(j)
        if i in j:
            appropriate.append(i)
        else:
            inappropriate.append(i)
        total = total + 1  
    #print(appropriate)

def sentiment(lines):
    polarity = []
    message = []
    dummy = []
    for line in lines:
       dummy.append(line.strip("\n"))
    for line in dummy:
        pol, mes = line.split("#",1)
        polarity.append(pol)
        message.append(mes)
    for i in range(len(polarity)):
        print(polarity[i], message[i])

def linking(lines):
    dummy = []
    videoid = []
    urls = []
    for line in lines:
        dummy.append(line.strip("\n"))
    for line in dummy:
        id, url = line.split("#", 1)
        videoid.append(id)
        urls.append(url)
    res = pd.DataFrame({'videoname': videoid, 'hyperlink': urls},columns = {'videoname', 'hyperlink'})
    urlcount = res.groupby('videoname')['hyperlink'].sum()
    print(urlcount.head(5)) 


Main()
