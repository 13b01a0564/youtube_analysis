import sys

def Main():
	data = getdata()
	accidentarea(data['accnt'])
	peakhour(data['peak'])
	busyroad(data['busy'])	

def getdata():
	map_data = {'accnt':[], 'peak':[], 'busy' :[]}
	for line in sys.stdin:
		finding, list = line.split("#", 1)
		if map_data.has_key(finding):
			map_data[finding].append(list)
	return map_data

def accidentarea(map_data):
	maxlist = []
	keys = []
	list = []
	fcoldata = []
	scoldata = []
	second = []
	keys = map_data		
	for line in keys:
		maxlist.append(line)
	for line in maxlist:
		a, b = line.split("\t")
		fcoldata.append(a)
		scoldata.append(b)
	for sco in scoldata:
		sco = int(sco)
		second.append(sco)
	maxvalue = max(second)
	i = second.index(maxvalue)
	print("\n")
	print("Accident prone area is:")
	print fcoldata[i], second[i]
	print("\n")

def peakhour(peaklist):
	mylist = []
	keys = []
	for i in peaklist:
		a, b = (i.strip("\n").split("\t"))
		mylist.append(b)
	my = []
	j = 0
	our = []
	for i in mylist:
		if j < 24:
			our.append(i)
			j += 1
		if j == 24:
			my.append(our)
			j = 0
			our = []
			#our.append(i)
	totlist = []	
	for j in range(0, 23):
		t = 0
		for i in range(1, len(my)):
			t += int(my[i][j])
		totlist.append(t)
	maxvalue = max(totlist)
	n = totlist.index(maxvalue)
	print ("The peak hour is:" + my[0][n])	
	print("\n")

def busyroad(busylist):
 	mylist = []
	totlist = []
	for i in busylist:
		a, b = i.strip("\n").split("\t") 	
		mylist.append(b)
	my = []
	j = 0
	our = []
	#print mylist
	for i in mylist:
		if j < 29:
			our.append(i)
			j += 1
		if j == 29:
			my.append(our)
			j = 0
			our = []
			totlist = []
	#print my
	for i in range(1,len(my)):
		t = 0
		for j in range(5, 29):
			t += int(my[i][j])
		totlist.append(t)
	#print totlist		
	avg = sum(totlist) / float(len(totlist))
	vehicles = []
	for k in totlist:
		if k > avg:
			vehicles.append(k)
	vehicles.sort(reverse = True)
	vehicle = vehicles[0]
	for i in range(1,len(my)):
		t = 0
		for j in range(5,29):
			t += int(my[i][j])
			if t == vehicle:
				p = i
	
	print("The Busiest Road is:" + my[p][0])
	print("\n")
Main()
