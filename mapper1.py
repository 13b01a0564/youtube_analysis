import sys
import math

def Main():
    lines = getData()
    highrelevance(lines)

def getData():
    lines = []
    for line in sys.stdin:
	lines.append(line)
    return lines

def highrelevance(lines):
    video_id = []
    video_duration = []
    timeon_video = []
    total = 0
    for line in lines:
	words = line.split(",")
	video_id.append(words[0])
	video_duration.append(words[10])
	timeon_video.append(words[11])
    for i in video_duration:
	a = int(i)/int(timeon_video[total])
	print "{0}#{1}|{2}".format("percentage", video_id[total], a)
	total = total + 1

Main()
